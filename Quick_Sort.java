package Sorting;
import java.util.Arrays;
public class Quick_Sort {

		public static void main (String args [])
		{
			int pivot;
			int lowerIndex, upperIndex;
			int [] myList = new int [20];
			for (int i = 0; i < myList.length; ++i)
			{
				myList [i] = (int) (Math.random()*100);
			}
			System.out.println(Arrays.toString(myList));
			lowerIndex = 0;
			upperIndex = myList.length-1;
			quickSortArray(lowerIndex, upperIndex, myList);
			System.out.println(Arrays.toString(myList));
			
		}// end main
		
		public static void quickSortArray(int lower, int high, int [] listPassed)
		{
			if (lower >= high)
			{
				return;
			}
			int i = lower;
			int j = high;
			int mid = (lower+high)/2;
			int pivot = listPassed[mid];
			while (i <= j)
			{
				while (listPassed[i]< pivot)
				{
					i = i +1;
				}
				while(listPassed[j] > pivot)
				{
					j = j-1;
				}
				if(i<=j)
				{
					int temp = listPassed[i];
					listPassed[i] = listPassed[j];
					listPassed[j] = temp;
					++i;
					--j;
				}
			}
			if(lower < j)
			{
				quickSortArray(lower, j, listPassed);
			}
			if(high > i)
			{
				quickSortArray(i, high, listPassed);
			}
		}
}// end class
